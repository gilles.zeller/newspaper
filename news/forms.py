from django import forms
from .models import Category


class CategoryForm(forms.ModelForm):
    """
    Form for Category Model
    """

    class Meta:
        model = Category
        fields = ['name', 'description']
