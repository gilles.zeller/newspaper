from django.db import models
from django.contrib.auth.models import User


class Category(models.Model):
    """
    Model for categories.
    """
    name = models.CharField(max_length=50, null=False)
    description = models.TextField(null=False)
    created_at = models.DateTimeField(auto_now_add=True, null=False)
    updated_at = models.DateTimeField(auto_now=True, null=False)


class Article(models.Model):
    """
    Model for articles.
    """
    url_param = models.CharField(max_length=50, null=False)
    title = models.CharField(max_length=50, null=False)
    content = models.TextField(null=False)
    category = models.ForeignKey(Category, on_delete=models.RESTRICT)
    user = models.ForeignKey(User, on_delete=models.RESTRICT)
    created_at = models.DateTimeField(auto_now_add=True, null=False)
    updated_at = models.DateTimeField(auto_now=True, null=False)
