from django.urls import path
from . import views

urlpatterns = [
    path('categories', views.list_categories, name='list_categories'),
    path('category', views.create_category, name='create_category'),
    path('')
]
