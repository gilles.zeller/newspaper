from django.shortcuts import render, redirect
from django.contrib import messages
from .models import Category
from .forms import CategoryForm


def list_categories(request):
    """
  View to list all categories
  :param request:
  :return:
  """
    categories = Category.objects.all()
    return render(request, 'news/RD_category.html', {'categories': categories})


def create_category(request):
    form = CategoryForm()
    if request.method == 'POST':
        form = CategoryForm(request.POST)
        if form.is_valid():
            form.save()
            messages.success(request, 'Successfully created new category!')
            return redirect('list_categories')
    return render(request, 'news/CU_category.html', {'form': form})


def delete_category(request, category_id):
    category = Category.objects.get(id=category_id)
    category.delete()
